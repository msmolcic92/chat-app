import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ChatApp } from './app.component';

import {
  AlertProvider,
  AuthProvider,
  ChatProvider,
  FriendsProvider,
  ProfileProvider
} from '../providers';

import {
  ChatPageModule,
  FriendsPageModule,
  LoginPageModule,
  ProfilePageModule,
  RecentMessagesPageModule,
  ResetPasswordPageModule,
  SignUpPageModule,
  TabsPageModule
} from "../pages/modules";

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
    apiKey: "AIzaSyBAGUaTinCqEpzM_K9Z_3vb_EPtO2U1BrU",
    authDomain: "chat-app-b81d8.firebaseapp.com",
    databaseURL: "https://chat-app-b81d8.firebaseio.com",
    projectId: "chat-app-b81d8",
    storageBucket: "chat-app-b81d8.appspot.com",
    messagingSenderId: "921822666416"
};

@NgModule({
  declarations: [
    ChatApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(ChatApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ChatPageModule,
    FriendsPageModule,
    LoginPageModule,
    ProfilePageModule,
    RecentMessagesPageModule,
    ResetPasswordPageModule,
    SignUpPageModule,
    TabsPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ChatApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AlertProvider,
    AuthProvider,
    ChatProvider,
    FriendsProvider,
    ProfileProvider
  ]
})
export class AppModule {}
