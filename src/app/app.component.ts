import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from "angularfire2/auth";
import { LoginPage, TabsPage, ProfilePage } from "../pages";

@Component({
  templateUrl: 'app.html'
})
export class ChatApp {
  private rootPage: any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    afAuth: AngularFireAuth
  ) {

    // Checks if user is logged in and redirects
    // him to the appropriate page accordingly.
    const authObserver =
      afAuth.authState.subscribe(user => {
        if (user) {
          this.rootPage = user.displayName ? TabsPage : ProfilePage;
          authObserver.unsubscribe();
        } else {
          this.rootPage = LoginPage;
          authObserver.unsubscribe();
        }
      });

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

