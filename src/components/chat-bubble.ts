import {Component} from '@angular/core';
import { Message } from "../classes";

@Component({
  selector: 'chat-bubble',
  inputs: [
      'msg: message',
      'ownerUid: ownerUid',
      'friendDisplayName: friendDisplayName'],
  template:
  `
  <div class="chatBubble">
    <div class="chat-bubble {{ msg.uid === ownerUid ? 'right' : 'left' }}">
      <div class="message">{{msg.text}}</div>
      <div class="message-detail">
          <div style="font-weight:bold;">{{ (msg.uid === ownerUid ? 'You' : friendDisplayName ) }}</div>
          <div>{{ getMessageTime() | moment:"ago" | lowercase }}</div>
      </div>
    </div>
  </div>
  `
})
export class ChatBubble {

  msg: Message;
  ownerUid: string;
  friendDisplayName: string;

  constructor() {}

  getMessageTime(): Date {
      return new Date(Number(this.msg.timestamp));
  }
}