import { Friend } from "./friend";

export class LastMessage {
    friend: Friend;
    text: string;
    timestamp: string;
}