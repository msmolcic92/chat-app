export class Message {
    uid: string;
    text: string;
    timestamp: string;
}