export class FriendRequest {
    uid: string;
    firstName: string;
    lastName: string;
}