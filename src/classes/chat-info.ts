import { Friend, Message } from "./";

export class ChatInfo {
    chatKey: string;
    uid: string;
    friend: Friend;
    messages: Message[];
}