export class Friend {
    uid: string;
    firstName: string;
    lastName: string;
}