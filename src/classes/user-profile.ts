export class UserProfile {
    uid: string;
    firstName: string;
    lastName: string;
}