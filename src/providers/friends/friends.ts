import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import { AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase/app';

import { Friend, FriendRequest, UserProfile } from '../../classes';
import { AuthProvider } from "../";

@Injectable()
export class FriendsProvider {

  private authData: AuthProvider;
  private afDb: AngularFireDatabase;
  
  constructor(
    authData: AuthProvider,
    afDb: AngularFireDatabase
  ) {
    this.authData = authData;
    this.afDb = afDb;
  }

  acceptFriendRequest(friendUid: string): firebase.Promise<void> {
    let currentUser = this.authData.getCurrentUser();

    let bulkUpdate = {};
    
    // Delete a friend request.
    bulkUpdate[`friend-requests/${currentUser.uid}/${friendUid}`] = null;

    // Add users as friends.
    bulkUpdate[`friends/${currentUser.uid}/${friendUid}`] = true;
    bulkUpdate[`friends/${friendUid}/${currentUser.uid}`] = true;

    // Create a chat handle for both users.
    let chatKey = this.afDb.database.ref('messages').push().key;

    bulkUpdate[`chats/${currentUser.uid}/${friendUid}`] = chatKey;
    bulkUpdate[`chats/${friendUid}/${currentUser.uid}`] = chatKey;

    return this.afDb
      .object('/')
      .update(bulkUpdate)
      .catch(error =>
        this.afDb
          .object(`messages/${chatKey}`)
          .remove()
      );
  }

  addFriend(friendUid: string): firebase.Promise<void> {
    let currentUser = this.authData.getCurrentUser();
    
    return this.afDb
      .object(`friend-requests/${friendUid}/${currentUser.uid}`)
      .set(true);
  }

  declineFriendRequest(friendUid: string): firebase.Promise<void> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .object(`friend-requests/${currentUser.uid}/${friendUid}`)
      .remove();
  }

  getFriendList(): Observable<Friend[]> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .list(`friends/${currentUser.uid}`)
      .map(friends => {
        let allFriends = new Array<Friend>();

        friends.forEach(friend => {
          this.afDb
            .object(`users/${friend.$key}`)
            .first()
            .subscribe(user => {
              allFriends.push({
                uid: user.$key,
                firstName: user.firstName,
                lastName: user.lastName
              });
            });
        });

        return allFriends;
      });
  }

  getFriendRequests(): Observable<FriendRequest[]> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .list(`friend-requests/${currentUser.uid}`)
      .map(friendRequests => {
        let allRequests = new Array<FriendRequest>();

        friendRequests.forEach(friendRequest => {
          this.afDb
            .object(`users/${friendRequest.$key}`)
            .first()
            .subscribe(user => {
              allRequests.push({
                uid: user.$key,
                firstName: user.firstName,
                lastName: user.lastName
              });
            });
        });

        return allRequests;
      });
  }

  getFriendRequestCount(): Observable<number> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .list(`friend-requests/${currentUser.uid}`)
      .map(friendRequests => friendRequests.length);
  }

  getNonFriends(): Observable<UserProfile[]> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .list(`friends/${currentUser.uid}`)
      .map(friends => friends.map(friend => friend.$key))
      .flatMap(friendUids => {
        friendUids.push(currentUser.uid);
        return this.afDb
          .list('users')
          .map(users => {
            return users
              .filter(user => friendUids.indexOf(user.$key) === -1)
              .map(user => {
                let result = new UserProfile();
                result.uid = user.$key;
                result.firstName = user.firstName;
                result.lastName = user.lastName;

                return result;
              });
          });
      });
  }

  removeFriend(friendUid: string): firebase.Promise<void> {
    let currentUser = this.authData.getCurrentUser();

    let bulkDelete = {};

    // Remove users from friend lists.
    bulkDelete[`friends/${currentUser.uid}/${friendUid}`] = null;
    bulkDelete[`friends/${friendUid}/${currentUser.uid}`] = null;

    return this.afDb.object('/').update(bulkDelete);
  }

}
