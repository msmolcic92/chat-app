import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable } from "angularfire2/database";

import firebase from 'firebase/app';

import { AuthProvider } from "../";
import { UserProfile } from '../../classes';

@Injectable()
export class ProfileProvider {

  private authData: AuthProvider;
  private afDb: AngularFireDatabase;

  constructor(
    authData: AuthProvider,
    afDb: AngularFireDatabase
  ) {
    this.authData = authData;
    this.afDb = afDb;
  }

  getUserProfile(): FirebaseObjectObservable<UserProfile> {
    let currentUser = this.authData.getCurrentUser();
    return this.afDb.object(`users/${currentUser.uid}`);
  }

  saveUserProfile(profile: UserProfile): firebase.Promise<void> {
    let currentUser = this.authData.getCurrentUser();
    return this.afDb
        .object(`users/${currentUser.uid}`)
        .update(profile);
  }

}
