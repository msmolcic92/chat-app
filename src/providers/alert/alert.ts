import { Injectable } from '@angular/core';
import { AlertController } from "ionic-angular";

@Injectable()
export class AlertProvider {

  constructor(public alertCtrl: AlertController) {}

  confirmAction(
    title: string,
    message: string,
    confirmationHandler: () => void
  ) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          role: 'Cancel'
        },
        {
          text: 'Confirm',
          handler: confirmationHandler
        }
      ]
    });

    alert.present();
  }

  displaySuccess(
    message: string,
    handler: () => void = null
  ) {
    let alert = this.alertCtrl.create({
      title: 'Success!',
      message: message,
      buttons: [{
        text: 'Ok',
        role: 'Cancel',
        handler: handler
      }]
    });

    alert.present();
  }

  displayError(error: Error) {
    let alert = this.alertCtrl.create({
      title: 'Whoooops!',
      message: error.message,
      buttons: [{
        text: 'Ok',
        role: 'Cancel'
      }]
    });

    alert.present();
  }

}
