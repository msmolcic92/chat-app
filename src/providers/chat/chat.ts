import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

import { AngularFireDatabase } from "angularfire2/database";
import firebase from 'firebase/app';

import { AuthProvider } from "../";
import { Message, ChatInfo, Friend, LastMessage } from "../../classes";

@Injectable()
export class ChatProvider {
  
  private authData: AuthProvider;
  private afDb: AngularFireDatabase;

  constructor(
    authData: AuthProvider,
    afDb: AngularFireDatabase
  ) {
    this.authData = authData;
    this.afDb = afDb;
  }

  sendMessage(chatKey: string, message: Message): firebase.Promise<void> {
    return this.afDb
      .list(`messages/${chatKey}`)
      .push(message);
  }

  getMessages(ngUnsubscribe: Subject<void>): Observable<LastMessage[]> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .list(`chats/${currentUser.uid}`)
      .map(friendKeys => {
        return friendKeys.map(friendKey => {
          let lastMessage = new LastMessage();

          this.afDb
            .object(`users/${friendKey.$key}`)
            .takeUntil(ngUnsubscribe)
            .subscribe(user => {
              let friend = new Friend();
              friend.uid = user.$key;
              friend.firstName = user.firstName;
              friend.lastName = user.lastName;
              lastMessage.friend = friend;
            });

          this.afDb
            .object(`chats/${currentUser.uid}/${friendKey.$key}`)
            .takeUntil(ngUnsubscribe)
            .subscribe(chatKey => {
              this.afDb
                .list(`messages/${chatKey.$value}`, {
                  query: {
                    limitToLast: 1 
                  }
                })
                .takeUntil(ngUnsubscribe)
                .subscribe(message => {
                  if (message && message.length) {
                    lastMessage.text = message[0].text;
                    lastMessage.timestamp = message[0].timestamp;
                  }
                });
            });

            return lastMessage;
        });
      });
  }

  openChat(friendUid: string, ngUnsubscribe: Subject<void>): Observable<ChatInfo> {
    let currentUser = this.authData.getCurrentUser();

    return this.afDb
      .object(`chats/${currentUser.uid}/${friendUid}`)
      .map(chatKey => {
        let chatData = new ChatInfo();
        chatData.chatKey = chatKey.$value;
        chatData.uid = currentUser.uid;

        this.afDb
          .object(`users/${friendUid}`)
          .takeUntil(ngUnsubscribe)
          .subscribe(user => {
            let friend = new Friend();
            friend.uid = user.$key;
            friend.firstName = user.firstName;
            friend.lastName = user.lastName;
            chatData.friend = friend;
          });

        this.afDb
          .list(`messages/${chatKey.$value}`)
          .takeUntil(ngUnsubscribe)
          .subscribe(messages => {
            chatData.messages = messages;
          });

        return chatData;
      });
  }

}
