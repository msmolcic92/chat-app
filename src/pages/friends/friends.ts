import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Subject } from "rxjs/Subject";

import { FriendsProvider, AlertProvider } from "../../providers";
import { UserProfile, FriendRequest, Friend } from "../../classes";
import { ChatPage } from "../";

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  private friends: Friend[];
  private friendRequests: FriendRequest[];
  private users: UserProfile[];
  
  currentTab: string = 'friend-list';
  
  constructor(
    public navCtrl: NavController,
    public alertData: AlertProvider,
    public friendsData: FriendsProvider
  ) {}

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
    this.friendsData
      .getFriendList()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(friends => this.friends = friends);

    this.friendsData
      .getFriendRequests()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(friendRequests => this.friendRequests = friendRequests);

    this.friendsData
      .getNonFriends()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(users => this.users = users);
  }

  acceptFriendRequest(friendRequest: FriendRequest): void {
    this.alertData.confirmAction(
      'Confirm friendship',
      `Are you sure you want to add ${friendRequest.firstName} ${friendRequest.lastName} as a friend?`,
      () => {
        this.friendsData
          .acceptFriendRequest(friendRequest.uid)
          .catch(error => this.alertData.displayError(error));
      }
    )
  }

  addFriend(user: UserProfile): void {
    this.alertData.confirmAction(
      'Add as friend',
      `Are you sure you want to send a friend request to ${user.firstName} ${user.lastName}?`,
      () => {
        this.friendsData
          .addFriend(user.uid)
          .then(
            () => this.alertData.displaySuccess('Friend request sent.'),
            error => this.alertData.displayError(error)
          );
      }
    );
  }

  declineFriendRequest(friendRequest: FriendRequest): void {
    this.alertData.confirmAction(
      'Decline friendship',
      `Are you sure you want to decline friend request from ${friendRequest.firstName} ${friendRequest.lastName}?`,
      () => {
        this.friendsData
          .declineFriendRequest(friendRequest.uid)
          .catch(error => this.alertData.displayError(error));
      }
    );
  }

  openChat(friend: Friend) {
    this.navCtrl.push(ChatPage, {
      friendUid: friend.uid
    });
  }

  removeFriend(friend: Friend) {
    this.alertData.confirmAction(
      'Remove from friend list',
      `Are you sure you want to remove ${friend.firstName} ${friend.lastName} from your friend list?`,
      () => {
        this.friendsData
          .removeFriend(friend.uid)
          .catch(error => this.alertData.displayError(error));
      }
    );
  }

}
