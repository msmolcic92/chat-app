import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Subject } from "rxjs/Subject";

import { TabsPage } from "../";
import {
  ProfileProvider,
  AuthProvider,
  AlertProvider
} from "../../providers";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  private profileForm: FormGroup;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public authData: AuthProvider,
    public profileData: ProfileProvider,
    public alertData: AlertProvider,
    public loadingCtrl: LoadingController
  ) {
    this.profileForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
    this.profileData
      .getUserProfile()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(profile => {
        this.profileForm.patchValue({
          firstName: profile.firstName,
          lastName: profile.lastName
        });
      });
  }

  save(): void {
    if (!this.profileForm.valid) {
      return;
    }
    
    this.profileData
      .saveUserProfile(this.profileForm.value)
      .then(
        () => {
          let currentUser = this.authData.getCurrentUser();

          if (!currentUser.displayName) {
            currentUser.updateProfile({
              displayName: this.profileForm.value.firstName,
              photoURL: null
            })
            .then(
              () =>
                this.alertData.displaySuccess(
                  'Successfully saved profile data.',
                  () => this.navCtrl.setRoot(TabsPage))
              ,
              error =>
                this.loading
                  .dismiss()
                  .then(() => this.alertData.displayError(error))
            );
          } else {
            this.loading
              .dismiss()
              .then(() => this.alertData.displaySuccess('Successfully saved profile data.'));
          }
        },
        error =>
          this.loading
            .dismiss()
            .then(() => this.alertData.displayError(error))
      );

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    
    this.loading.present();
  }

}
