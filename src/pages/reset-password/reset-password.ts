import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthProvider, AlertProvider } from '../../providers';
import { EmailValidator } from '../../validators';

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  private resetPasswordForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public authData: AuthProvider,
    public formBuilder: FormBuilder,
    public alertData: AlertProvider
  ) {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        EmailValidator.isValid
      ])]
    });
  }

  resetPassword(): void {
    if (!this.resetPasswordForm.valid) {
      return;
    }

    this.authData
      .resetPassword(this.resetPasswordForm.value.email)
      .then(
        user =>
          this.alertData.displaySuccess(
            'An email with reset link has been sent to your address.',
            () => this.navCtrl.pop())
        ,
        error => this.alertData.displayError(error)
      );
  }

}
