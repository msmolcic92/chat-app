import { Component } from '@angular/core';

import {
  IonicPage,
  NavController,
  LoadingController,
  Loading
} from 'ionic-angular';

import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
 
import { AuthProvider, AlertProvider } from '../../providers';
import { EmailValidator } from '../../validators';
import {
  ResetPasswordPage,
  SignUpPage,
  ProfilePage,
  TabsPage
} from "../";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private loginForm: FormGroup;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public authData: AuthProvider,
    public formBuilder: FormBuilder,
    public alertData: AlertProvider,
    public loadingCtrl: LoadingController
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        EmailValidator.isValid
      ])],
      password: ['', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])]
    });
  }

  goToResetPassword(): void {
    this.navCtrl.push(ResetPasswordPage);
  }

  signUp(): void {
    this.navCtrl.push(SignUpPage);
  }

  login(): void {
    if (!this.loginForm.valid) {
      return;
    }

    this.authData
      .login(
        this.loginForm.value.email,
        this.loginForm.value.password)
      .then(
        () => {
          let currentUser = this.authData.getCurrentUser();
          
          if (currentUser.displayName == null) {
            this.navCtrl.setRoot(ProfilePage);
          } else {
            this.navCtrl.setRoot(TabsPage);
          }
        },
        error =>
          this.loading
            .dismiss()
            .then(() => this.alertData.displayError(error))
      );

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    
    this.loading.present();
  }

}
