import { Component } from '@angular/core';

import {
  IonicPage,
  NavController,
  LoadingController,
  Loading
} from 'ionic-angular';

import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
 
import { AuthProvider, AlertProvider } from '../../providers';
import { EmailValidator } from '../../validators';
import { ProfilePage } from "../";

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  private signUpForm: FormGroup;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public authData: AuthProvider,
    public formBuilder: FormBuilder,
    public alertData: AlertProvider,
    public loadingCtrl: LoadingController
  ) {
    this.signUpForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        EmailValidator.isValid
      ])],
      password: ['', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])]
    });
  }

  signUp(): void {
    if (!this.signUpForm.valid) {
      return;
    }
    
    this.authData
      .signUp(
        this.signUpForm.value.email,
        this.signUpForm.value.password)
      .then(
        () => this.navCtrl.setRoot(ProfilePage),
        error => {
          this.loading
            .dismiss()
            .then(() => this.alertData.displayError(error));
        });

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    
    this.loading.present();
  }

}
