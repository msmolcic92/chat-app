export * from './chat/chat.module';
export * from './friends/friends.module';
export * from './login/login.module';
export * from './profile/profile.module';
export * from './recent-messages/recent-messages.module';
export * from './reset-password/reset-password.module';
export * from './sign-up/sign-up.module';
export * from './tabs/tabs.module';