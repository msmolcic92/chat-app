import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecentMessagesPage } from './recent-messages';

@NgModule({
  declarations: [
    RecentMessagesPage
  ],
  imports: [
    IonicPageModule.forChild(RecentMessagesPage)
  ],
  exports: [
    RecentMessagesPage
  ]
})
export class RecentMessagesPageModule {}
