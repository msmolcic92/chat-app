import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Subject } from "rxjs/Subject";
import moment from 'moment';

import { ChatProvider } from "../../providers";
import { LastMessage } from "../../classes";
import { ChatPage } from "../";

@IonicPage()
@Component({
  selector: 'page-recent-messages',
  templateUrl: 'recent-messages.html',
})
export class RecentMessagesPage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  private messages: LastMessage[];

  constructor(
    public navCtrl: NavController,
    public chatData: ChatProvider
  ) {}

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
      this.chatData
        .getMessages(this.ngUnsubscribe)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(messages => this.messages = messages);
  }

  getTimestamp(timestamp: string): string {
    if (!timestamp)
      return '';

    return moment(Number(timestamp)).format("MMM DD YY' HH:mm:ss");
  }

  openMessage(message: LastMessage): void {
    this.navCtrl.push(ChatPage, {
      friendUid: message.friend.uid
    });
  }

}
