import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController } from "ionic-angular";
import { Subject } from "rxjs/Subject";

import {
  RecentMessagesPage,
  ProfilePage,
  FriendsPage,
  LoginPage
} from "../";

import {
  AuthProvider,
  AlertProvider,
  FriendsProvider
} from "../../providers";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage implements OnDestroy, OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  private friendRequestsCount: number;

  RecentMessagesRoot = RecentMessagesPage;
  FriendsRoot = FriendsPage;
  ProfileRoot = ProfilePage;

  constructor(
    public authData: AuthProvider,
    public alertData: AlertProvider,
    public friendsData: FriendsProvider,
    public navCtrl: NavController
  ) {}

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
    this.friendsData
      .getFriendRequestCount()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(count => this.friendRequestsCount = count);
  }

  logout(): void {
    this.alertData.confirmAction(
      'Confirm Logout',
      'Are you sure you want to logout?',
      () => {
        this.navCtrl
          .setRoot(LoginPage)
          .then(
            () => this.authData.logout(),
            error => this.alertData.displayError(error)
          );
      });
  }

}
