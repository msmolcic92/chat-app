import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPage } from './chat';
import { ChatBubble } from "../../components/chat-bubble";
import { MomentPipe } from "../../pipes/moment.pipe";

@NgModule({
  declarations: [
    ChatPage,
    ChatBubble,
    MomentPipe
  ],
  imports: [
    IonicPageModule.forChild(ChatPage)
  ],
  exports: [
    ChatPage
  ]
})
export class ChatPageModule {}
