import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, List } from 'ionic-angular';
import { Subject } from "rxjs/Subject";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { ChatInfo, Message } from "../../classes";
import { ChatProvider, AlertProvider } from "../../providers";
import { AngularFireDatabase } from "angularfire2/database";

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class ChatPage implements OnDestroy, OnInit {
  @ViewChild(Content) content: Content;
  @ViewChild(List) list: List;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  private chatInfo: ChatInfo;
  private messageForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertData: AlertProvider,
    public chatData: ChatProvider,
    public formBuilder: FormBuilder,
    public afDb: AngularFireDatabase
  ) {
    this.messageForm = this.formBuilder.group({
      text: ['', Validators.required]
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
    let friendUid = this.navParams.get('friendUid');

    this.chatData
      .openChat(friendUid, this.ngUnsubscribe)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(chatInfo => {
        this.chatInfo = chatInfo;

        // Move scroll to bottom on each new message.
        this.afDb.database
          .ref(`messages/${this.chatInfo.chatKey}`)
          .limitToLast(1)
          .on('child_added', snapshot => {
            this.scrollToBottom(1000);
          });
      });
  }

  ionViewDidEnter(): void {
    this.scrollToBottom();
  }

  scrollToBottom(duration: number = 0): void {
    try {
      this.content.scrollTo(0, this.list._elementRef.nativeElement.offsetHeight, duration);
    } catch (error) {}
  }

  sendMessage(): void {
    if (!this.messageForm.valid) {
      return;
    }

    let message = new Message();
    message.uid = this.chatInfo.uid;
    message.text = this.messageForm.value.text;
    message.timestamp = Date.now().toString();

    this.chatData
      .sendMessage(this.chatInfo.chatKey, message)
      .then(
        () => this.messageForm.reset(),
        error => this.alertData.displayError(error));
  }

}
